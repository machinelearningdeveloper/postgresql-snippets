#!/usr/bin/env bash

set -euo pipefail

if [[ "${USER}" != 'root' ]]; then
  echo "Error: you must run ${0} as root" >&2
  exit 1
fi

if [ -z "${SITE_ADMIN_USER:-}" ]; then
  echo "Error: you must set SITE_ADMIN_USER" >&2
  exit 1
fi

TIMESTAMP=$(date +'%Y%m%d%H%M%S')

PG_HBA_CONF_FILE=/var/lib/pgsql/10/data/pg_hba.conf
PG_HBA_CONF_BACKUP="${PG_HBA_CONF_FILE}.backup.${TIMESTAMP}"
POSTGRESQL_CONF_FRAGMENT_FILE="/var/lib/pgsql/10/data/conf.d/${TIMESTAMP}.conf"

rsync -av "${PG_HBA_CONF_FILE}" "${PG_HBA_CONF_BACKUP}"
echo "local all all trust" > "${PG_HBA_CONF_FILE}"

sudo -u postgres echo $'log_statement = none\nlog_min_duration_statement = -1' | tee "${POSTGRESQL_CONF_FRAGMENT_FILE}"

systemctl restart postgresql-10

echo
echo "Change Password"
echo "==============="
echo -n "User: "
read user
echo -n "Password: "
read -s password
echo
echo -n "Verify password: "
read -s verify

if [[ "${password}" == "${verify}" ]]; then
  sudo -u postgres psql -d postgres -U "${SITE_ADMIN_USER}" -c "ALTER ROLE ${user} WITH PASSWORD '${password}'"
  echo
  echo "Password changed for ${user}."
else
  echo
  echo "Error: passwords do not match" >&2
fi

rsync -av "${PG_HBA_CONF_BACKUP}" "${PG_HBA_CONF_FILE}"
rm -v "${POSTGRESQL_CONF_FRAGMENT_FILE}"

systemctl restart postgresql-10
